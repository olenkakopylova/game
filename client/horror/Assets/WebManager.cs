﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Net.WebSockets;
using System.Threading.Tasks;
using System.Threading;
using System;
using System.Text;

public class WebManager
{

    private string _uri;

    /// <summary>Use to </summary>
    private ClientWebSocket _client;
    private int _bufferSize = 1500;//???


    public WebManager()
    {
        //_uri = AppContext.Uri.GetUri();
        _uri = "ws://1ea75505.ngrok.io";
        Task t = Connect();
    }

    public async void CloseConnection()
    {
        await _client.CloseAsync(WebSocketCloseStatus.NormalClosure, string.Empty, CancellationToken.None);
    }

    private async Task Connect()
    {
        _client = new ClientWebSocket();
        await _client.ConnectAsync(new Uri(_uri), CancellationToken.None);
        await GetAnswer();
    }

    public async Task SendMessage<T>(T authorizeMessage)
    {
        string jsonAuthorize = AppContext.Json.WriteToJson(authorizeMessage);
        ArraySegment<byte> outputAuthorizeData = new ArraySegment<byte>(Encoding.UTF8.GetBytes(jsonAuthorize));
            await _client.SendAsync(outputAuthorizeData, WebSocketMessageType.Text, true, CancellationToken.None);
    }

    private async Task GetAnswer()
    {
        try
        {
            while (_client.State == WebSocketState.Open)
            {
                byte[] buffer = new byte[_bufferSize];
                var result = await _client.ReceiveAsync(new ArraySegment<byte>(buffer), CancellationToken.None);
                var resultJson = (new UTF8Encoding()).GetString(buffer);
                AppContext.Json.WriteFromJson(resultJson);
            }
            if (!(_client.State == WebSocketState.Open))
            {
                await _client.ConnectAsync(new Uri(_uri), CancellationToken.None);
            }
        }
        catch
        {
        }


    }
}