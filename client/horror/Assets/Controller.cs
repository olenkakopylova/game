﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller : MonoBehaviour {

	public Transform magic;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown ("a")||Input.GetKey(KeyCode.JoystickButton1)) {
			Vector3 positionMagic = new Vector3 (this.transform.position.x, this.transform.position.y+5, this.transform.position.z+5);
			Transform m = Instantiate(magic,positionMagic, Quaternion.identity) as Transform; 
			SendDamage (0);
		}
	}

	private async void SendDamage(int i){
		await AppContext.WebManager.SendMessage (new Damage (i));
	}
}
