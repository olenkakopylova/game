﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Loader : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public static GameObject Load(string name){
		return Instantiate (Resources.Load (name, typeof(GameObject)), AppContext.enemyPosition, Quaternion.identity) as GameObject;
	}
}	
