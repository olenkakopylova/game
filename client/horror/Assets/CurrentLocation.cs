﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class CurrentLocation {

    public string type = "currentEnemyLocation";
    public float x;
    public float y;
    public float z;
    public float rotX;
    public float rotY;
    public float rotZ;
}
