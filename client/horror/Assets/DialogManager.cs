﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogManager {

    private GameObject _preloader;
    private GameObject _finish;
	private GameObject _player;

    public void ShowPreloader()
    {
        _preloader.SetActive(true);
    }

    public void HidePreloader()
    {
        _preloader.SetActive(false);
    }

    public void ShowFinish()
    {
        _finish.SetActive(true);
    }

    public void HideFinish()
    {
        _finish.SetActive(false);
    }

	public void ShowPlayer(){
		_player.SetActive (true);
	}

	public void HidePlayer(){
		_player.SetActive (false);
	}

	public DialogManager()
    {
        _preloader = GameObject.Find("Preloader");
        _finish = GameObject.Find("Finish");
		_player = GameObject.Find ("GameObject 1");

		_player.SetActive (false);
        _preloader.SetActive(false);
        _finish.SetActive(false);
    }
}
