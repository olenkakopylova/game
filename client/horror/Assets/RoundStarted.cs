﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class RoundStarted {

    public string type = "roundStart";
    public float x;
    public float y;
    public float z;
	public float xEnemy;
	public float yEnemy;
	public float zEnemy;
}
